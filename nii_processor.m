%==========================================================================
% author            Pouria Hadjibagheri
% last modified     26 November 2015
% matlab version    MATLAB R2015b
% Licence			GPLv2.0
%==========================================================================

function varargout = nii_processor(varargin)
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @nii_processor_OpeningFcn, ...
                   'gui_OutputFcn',  @nii_processor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before nii_processor is made visible.
function nii_processor_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to nii_processor (see VARARGIN)
file_path = 'CT_vol.nii';
image_matrix = nii_read(file_path);
spacial_dims = size(image_matrix);

% Discarding singular dimensions.
%Set the slice and view here.
img_slice = squeeze(image_matrix(270, :, :));

transposed_slice = transpose(img_slice);
adjusted_view = flipud(transposed_slice);

imagesc(adjusted_view);
colormap(gray);
% Choose default command line output for nii_processor
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



% --- Outputs from this function are returned to the command line.
function varargout = nii_processor_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
file_path = 'CT_vol.nii';
image_matrix = nii_read(file_path);
dims = size(image_matrix);

img_slice = squeeze(image_matrix(round(dims(1)*get(hObject, 'Value')), ...
    :, :));

transposed_slice = transpose(img_slice);
adjusted_view = flipud(transposed_slice);

imagesc(adjusted_view);
colormap(gray);


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if isequal(get(hObject,'BackgroundColor'), get(0,...
        'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

file_path = 'CT_vol.nii';
image_matrix = nii_read(file_path);
dims = size(image_matrix);

img_slice = squeeze(image_matrix(:, round(dims(2)*get(hObject, 'Value'))...
    , :));

transposed_slice = transpose(img_slice);
adjusted_view = flipud(transposed_slice);

imagesc(adjusted_view);
colormap(gray);

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,...
        'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

file_path = 'CT_vol.nii';
image_matrix = nii_read(file_path);
dims = size(image_matrix);

img_slice = squeeze(image_matrix(:, :, round(dims(3)*get(hObject, ...
    'Value'))));

transposed_slice = transpose(img_slice);
adjusted_view = flipud(transposed_slice);

imagesc(adjusted_view);
colormap(gray);

% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if isequal(get(hObject,'BackgroundColor'), get(0,...
        'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
