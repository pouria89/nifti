%==========================================================================
% author            Pouria Hadjibagheri
% last modified     26 November 2015
% matlab version    MATLAB R2015b
% Licence			GPLv2.0
%==========================================================================

%%
clc; 
clear; 
clearvars all; 

file_path = 'CT_vol.nii';
image_matrix = nii_read(file_path);
spacial_dims = size(image_matrix);

% Discarding singular dimensions.
%Set the slice and view here.
img_slice = squeeze(image_matrix(270, :, :));
filter = fspecial('unsharp');
img_slice = imfilter(img_slice, filter, 'replicate');
transposed_slice = transpose(img_slice);
adjusted_view = flipud(transposed_slice);
%mask=im2bw(adjusted_view,graythresh(adjusted_view));
%mask=bwareaopen(mask,1);
filter = fspecial('gaussian',3);
%mask=imfilter(adjusted_view, filter, 'replicate');
mask = im2bw(imfilter(adjusted_view, filter, 'replicate'));
adjusted_view(mask<0.3)=NaN;
%adjusted_view = adjusted_view(~mask);
imagesc(adjusted_view);
colormap(gray);

%%
isonormals(smooth3(image_matrix)); 
patch('Vertices', verts, 'Faces', faces, ... 
    'FaceVertexCData', colors, ... 
    'FaceColor','interp', ... 
    'edgecolor', 'interp');
view(30,-15);
colormap parula;