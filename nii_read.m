function [ image_matrix ] = nii_read( path )
%NII_READ(path): Gets the path to a <.nii> NIFTI file as a Char object and 
%                returns a ready-to-render matrix of the image. 
%
%   REQUIRES: nii_info
%
%   INPUT
%   =======================================================================
%   path         Must be either an absolute path, or a relative path with 
%                respect to the location of the function / executable.
%
%
%   RETURN
%   =======================================================================
%   IMAGE_MATRIX Matrix of type double of the data retrieved from the NIFTI 
%                file ready for additional manipulation or rendering as an 
%                image. 
%
%==========================================================================
% author            Pouria Hadjibagheri
% last modified     26 November 2015
% matlab version    MATLAB R2015b
% licence	    GPLv2.0
%==========================================================================
    
    START_LOC = 'bof';
    % Standard length of NIFTI header.

    HEADER_LEN = 352;  % bytes
    
    % Note that @nii_info should be called before opening the file in this 
    % function to avoid conflicts and I/O error.
    [~, img_dats, data_type] = nii_info(path);
    
    try 
        file_data = fopen(path);
    catch exception
        error ('Unable to access file <%s>.\n', path);
    end
    
    % [2:4] correspond to the dimensions of the image, according to the docs.
    img_dims = img_dats(2:4);
    
    fseek(file_data, HEADER_LEN, START_LOC);
    voxels = fread(file_data, prod(img_dims), data_type); 
    fclose(file_data);
    
    % Reshape to the correct dimensions.
    % Returns [image_matrix].
    image_matrix = reshape(voxels, transpose(img_dims));
    
end

